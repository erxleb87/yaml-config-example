
from yaml import safe_load

# The keys used in the config file
# It is highly recommended to put them into constants
# + Easier to use with auto-completion
# + Guards a bit against typos
# + Easier to adapt if labels change
# If there are too many, consider putting them into a dedicated module

LABEL_TITLE = "title"
LABEL_AUTHOR = "author"
LABEL_NAME = "name"
LABEL_EMAIL = "email"
LABEL_DIFFICULTY = "difficulty"
LABEL_HALAL = "halal"
LABEL_VEGETARIAN = "vegetarian"
LABEL_SHOPPING = "shopping_list"
LABEL_ITEM = "item"
LABEL_AMOUNT = "amount"
LABEL_UNIT = "unit"

# === === ===

def read_configuration(from_file):
    """Read the configuration from a file and return the content within.

    The file is supposed to be in the YAML format

    Args:
        from_file: The file containing the configuration
    
    Returns:
        The configuration values as a dictionary
    """

    with open(from_file) as input_stream:
        configuration = safe_load(input_stream)  # Let the YAML library do the heavy lifting
    
    return configuration

def print_configuration(configuration):
    """Print the given configuration as a nice recipe.

    Args:
        configuration: the contents of the config file as a dictionary
    """

    is_halal = configuration[LABEL_HALAL]
    is_vegetarian = configuration[LABEL_VEGETARIAN]
    diet_info = ( 
        "not halal" if not is_halal else "halal" +
        ", " + "not vegetarian" if not is_vegetarian else "vegetarian"
    )

    author = configuration[LABEL_AUTHOR]  # author is a dictionary in itself

    print(configuration[LABEL_TITLE], "(", diet_info, ")")
    print("by", author[LABEL_NAME], "<" + author[LABEL_EMAIL] + ">")
    print("Difficulty:", "*" * configuration[LABEL_DIFFICULTY])
    print("=" * 80)
    for entry in configuration[LABEL_SHOPPING]:
        item = entry[LABEL_ITEM]
        amount = entry[LABEL_AMOUNT]
        unit = entry.get(LABEL_UNIT, "")  # Unit may be empty so use .get() to specify a default
        print("-", amount, unit, item)


if __name__ == "__main__":
    config_file = "pancake-config.yml"  # One could use pathlib.Path for improved file handling here
    configuration = read_configuration(config_file)
    print_configuration(configuration)

