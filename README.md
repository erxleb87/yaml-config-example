# YAML config example

This is an example on how to read a configuration file in the YAMl format with Python.

You can modify the `pancake-config.yml` or write your own configuration if you want.
To run the example, clone the repository or copy the `.py` and `.yml` files and run

```
python -m read_configuration_simple
```


### Notes on Improvement

One might consider to use classes to improve on the program.
For example the _recipe_ itself, the _author_ or a _shopping list item_ would be good candidates for classes.
Then the `read_configuration` could return an object hierarchy instead of a dictionary.
Separating the labels used in the configuration into a separate module might also be a good idea.
